package com.example.demo.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller

public class ForumController {

	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top(@ModelAttribute("start_date") String start, @ModelAttribute("end_date") String end) throws ParseException {
		ModelAndView mav = new ModelAndView();
		// 投稿を全件取得
		List<Report> contentData = reportService.findByCreatedDataBetween(start, end);
		//コメントを取得
		List<Comment> commentData = commentService.findByCreatedDataBetween(start, end);
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("comments", commentData);
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		//idをもとに投稿を削除
		reportService.deleteReport(id);
		//rootへリダイレクト
		return new ModelAndView("redirect:/");

	}

	//編集画面へ偏移
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = reportService.editReport(id);
		// 画面遷移先を指定
		mav.setViewName("/edit");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	//編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateConteng(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		// UrlParameterのidを更新するentityにセット
		report.setId(id);
		// 編集した投稿を更新
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//コメント画面へ偏移
	@GetMapping("/comment/{id}")
	public ModelAndView addContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = reportService.editReport(id);
		// 画面遷移先を指定
		mav.setViewName("/comment");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// コメント投稿処理
	@PostMapping("/addComment")
	public ModelAndView addComment(@ModelAttribute("formModel") Comment report) {
		// 投稿をテーブルに格納
		commentService.saveComment(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//コメント編集画面へ偏移
	@GetMapping("/editComment/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Comment report = commentService.editReport(id);
		// 画面遷移先を指定
		mav.setViewName("/editComment");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	//コメント編集処理
	@PutMapping("/editComment/{id}")
	public ModelAndView editComment(@PathVariable Integer id, @ModelAttribute("formModel") Comment report) {
		// UrlParameterのidを更新するentityにセット
		report.setId(id);
		// 編集した投稿を更新
		commentService.saveComment(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//削除処理
	@GetMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		//idをもとに投稿を削除
		commentService.deleteReport(id);
		//rootへリダイレクト
		return new ModelAndView("redirect:/");

	}
}
