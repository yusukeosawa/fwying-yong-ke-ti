package com.example.demo.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Controller
public class ReportService {
	@Autowired

	ReportRepository reportRepository;
	final String START_DATE_INITIAL_VALUE = "2020-01-01 00:00:00";

	// レコード全件取得
	public List<Report> findByCreatedDataBetween(String start, String end) throws ParseException {
		Date date = new Date();
		long time = date.getTime();
		Timestamp ts = new Timestamp(time);

		String startDate;
		String endDate;
		if ((start != null) && (!start.isEmpty())) {
			startDate = start + " 00:00:00";
		} else {
			startDate = START_DATE_INITIAL_VALUE;
		}
		if ((end != null) && (!end.isEmpty())) {
			endDate = end + " 23:59:59";
		} else {
			endDate = ts.toString();
		}
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDateTime = sdFormat.parse(startDate);
		Date endDateTime = sdFormat.parse(endDate);
		return reportRepository.findByCreatedDataBetweenOrderByCreatedDataDesc(startDateTime, endDateTime);
	}

	public List<Report> findAllByOrderByCreatedDataDesc() {
		return reportRepository.findAllByOrderByCreatedDataDesc();
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	//削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);

	}

	//レコード1件取得
	public Report editReport(Integer id) {
		Report report = reportRepository.findById(id).orElse(null);
		return report;
	}
}
